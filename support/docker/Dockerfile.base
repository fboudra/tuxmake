ARG BASE
FROM ${BASE}

ENV DEBIAN_FRONTEND=noninteractive

# Heavyweight: package installation
# This list of packages was seeded from kernelci's base image
# https://github.com/kernelci/kernelci-core/blob/master/jenkins/dockerfiles/build-base/Dockerfile
RUN apt-get update \
    && apt-get install auto-apt-proxy --assume-yes --no-install-recommends \
    && apt-get install --assume-yes --no-install-recommends --option=debug::pkgProblemResolver=yes \
        procps \
        apt-transport-https \
        ca-certificates \
        bash \
        bc \
        bison \
        bsdmainutils \
        bzip2 \
        ccache \
        cpio \
        flex \
        git \
        gzip \
        jq \
        kmod \
        libssl-dev \
        libelf-dev \
        lzop \
        make \
        rsync \
        tar \
        u-boot-tools \
        wget \
        xz-utils

# Lightweight: base container customizations
RUN useradd --create-home tuxmake

# vim: ft=dockerfile
